# cluster-ops-operator
[![Docker Repository on Quay](https://quay.io/repository/wanderb/cluster-ops-operator/status "Docker Repository on Quay")](https://quay.io/repository/wanderb/cluster-ops-operator)

An operator for deploying cluster prune cronjobs on an OpenShift cluster

## Deploy the operator


1. Create a project for the operator, and the resulting cronjobs.

  ```bash
  oc adm new-project cluster-ops
  oc project cluster-ops
  ```

2. Change the `namespace` in `deploy/clusterrole_binding.yaml` to match the
   namespace of the project you are deploying the operator into.

3. Create the following resources from the `deploy` directory, make sure you are a
   cluster admin, or else you will not have enough permissions to create all
   resources.

  ``` bash
  oc apply -f deploy/crds/resourceprune_v1_resourceprune_crd.yaml
  oc apply -f deploy
  ```

4. Create a new `Resourceprune` object, an example is given in
   `deploy/crds/resourceprune_v1_resourceprune_cr.yaml`

   ```bash
   oc apply -f - <<< EOF
   apiVersion: resourceprune.cluster-ops.hcs-company.com/v1
   kind: Resourceprune
   metadata:
     name: example-resourceprune
   spec:
     builds:
       schedule: "5 2 * * *"
       keepComplete: 5
       keepFailed: 1
       keepYoungerThan: "60m"
       pruneOrphans: "true"
     deployments:
       schedule: "15 2 * * *"
       keepComplete: 5
       keepFailed: 1
       keepYoungerThan: "60m"
       pruneOrphans: "true"
     images:
       schedule: "5 1 * * *"
       keepTagRevisions: 3
       keepYoungerThan: "1h0m0s"
       ignoreInvalidRefs: false
   EOF
   ```
   Any settings not defined in the `spec:` will use the defaults given above.
